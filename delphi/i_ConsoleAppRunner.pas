unit i_ConsoleAppRunner;

interface

type
  TOnOutputString = procedure(Sender: TObject; const ABuffer: string) of object;
  TOnProcessMessages = function(Sender: TObject): Boolean of object;

  IConsoleAppRunner = interface
    ['{00EF2BE9-9903-47D2-A0B9-C85FA7513337}']
    function Run(
      const ACommand: string;
      const AParameters: string;
      const ACurrentDirectory: string
    ): Cardinal;
  end;

implementation

end.
