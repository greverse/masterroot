from cx_Freeze import setup, Executable

build_exe_options = {'include_msvcr': True,
                     'excludes': ['hashlib', 'bz2', 'unittest', 'unicodedata'],
                     'build_exe': '../bin/MasterRootPy/'}

setup(options = {"build_exe": build_exe_options},
      executables = [Executable("MasterRootPy.py")])