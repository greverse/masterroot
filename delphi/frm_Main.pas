unit frm_Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvDirectoryEdit, AdvEdit, AdvEdBtn, AdvFileNameEdit,
  ComCtrls, i_ConsoleAppRunner;

type
  TfrmMain = class(TForm)
    pgc1: TPageControl;
    tsUnpack: TTabSheet;
    tsPack: TTabSheet;
    edtOpenFileUnpack: TAdvFileNameEdit;
    mmoLog: TMemo;
    diredtUnpack: TAdvDirectoryEdit;
    btnClearLog: TButton;
    btnRun: TButton;
    btnExit: TButton;
    lbl1: TLabel;
    lbl2: TLabel;
    diredtPack: TAdvDirectoryEdit;
    lbl3: TLabel;
    edtOutFilePack: TAdvFileNameEdit;
    lbl4: TLabel;
    cbbType: TComboBox;
    lbl5: TLabel;
    procedure btnClearLogClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FWorkDir: string;
    FConsole: IConsoleAppRunner;
    procedure OnOutputString(Sender: TObject; const ABuffer: string);
    function OnProcessMessages(Sender: TObject): Boolean;
    function GetFullPath(const ARelativePath: string): string;
    function GetFullName(const ARelativeName: string): string;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  ShLwApi,
  u_ConsoleAppRunner;

{$R *.dfm}

function TfrmMain.GetFullPath(const ARelativePath: string): string;
begin
  SetLength(Result, MAX_PATH);
  PathCombine(@Result[1], PChar(ExtractFilePath(FWorkDir)), PChar(ARelativePath));
  SetLength(Result, LStrLen(PChar(Result)));
  Result := IncludeTrailingPathDelimiter(Result);
end;

function TfrmMain.GetFullName(const ARelativeName: string): string;
var
  VFileName: string;
  VFilePath: string;
begin
  Assert(ARelativeName <> '');
  VFileName := ExtractFileName(ARelativeName);
  Assert(VFileName <> '');
  VFilePath := ExtractFilePath(ARelativeName);
  if VFilePath = '' then begin
    VFilePath := '.';
  end;
  Result := GetFullPath(VFilePath) + VFileName;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FWorkDir := ExtractFilePath(ParamStr(0));

  pgc1.ActivePageIndex := 0;
  cbbType.ItemIndex := 0;
  
  FConsole := TConsoleAppRunner.Create(
    False, // IsEchoOFF
    Self.OnOutputString,
    Self.OnProcessMessages
  );
end;

procedure TfrmMain.OnOutputString(Sender: TObject; const ABuffer: string);
begin
  mmoLog.Lines.BeginUpdate;
  mmoLog.Lines.Text := mmoLog.Lines.Text + StringReplace(ABuffer, #$D#$A#$D#$A, #$D#$A, [rfReplaceAll]);
  mmoLog.Lines.EndUpdate;
  mmoLog.Perform(EM_LINESCROLL, 0, mmoLog.Lines.Count - 1);
end;

function TfrmMain.OnProcessMessages(Sender: TObject): Boolean;
begin
  Application.ProcessMessages;
  Result := True;
end;

procedure TfrmMain.btnClearLogClick(Sender: TObject);
begin
  mmoLog.Clear;
end;

procedure TfrmMain.btnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.btnRunClick(Sender: TObject);
var
  VAppName: string;
  VCommand: string;
  VType: string;
  VFileName: string;
  VDirName: string;
begin
  VAppName := FWorkDir + 'MasterRootPy\MasterRootPy.exe';

  if not FileExists(VAppName) then begin
    MessageDlg('File not found: ' + VAppName, mtError, [mbOK], 0);
    Exit;
  end;

  case pgc1.ActivePageIndex of

    0: begin // Unpack

      VFileName := Trim(edtOpenFileUnpack.Text);
      if VFileName <> '' then begin
        VFileName := GetFullName(VFileName);
      end;
      if not FileExists(VFileName) then begin
        MessageDlg('Please, select input dbRoot.v5 file first!', mtError, [mbOK], 0);
        Exit;
      end;

      VDirName := Trim(diredtUnpack.Text);
      if VDirName = '' then begin
        MessageDlg('Please, set output folder first!', mtError, [mbOK], 0);
        Exit;
      end;

      VCommand := 'unpack' + ' ' + VFileName + ' ' + GetFullPath(VDirName);
    end;

    1: begin // Pack

      case cbbType.ItemIndex of
        0: VType := '--xml';
        1: VType := '--proto';
      else
        Assert(False);
      end;

      VFileName := Trim(edtOutFilePack.Text);
      if VFileName = '' then begin
        MessageDlg('Please, set output file name first!', mtError, [mbOK], 0);
        Exit;
      end else begin
        VFileName := GetFullName(VFileName);
      end;

      VDirName := Trim(diredtPack.Text);
      if (VDirName = '') or not DirectoryExists(GetFullPath(VDirName)) then begin
        MessageDlg('Please, set folder with components first!', mtError, [mbOK], 0);
        Exit;
      end;

      VCommand := 'pack' + ' ' + VType + ' ' + GetFullPath(VDirName) + ' ' + VFileName;
    end;
  else
    Assert(False);
  end;

  mmoLog.Clear;

  btnRun.Enabled := False;

  FConsole.Run(VAppName, VCommand, FWorkDir);

  btnRun.Enabled := True;
end;

end.
