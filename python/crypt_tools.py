#!/usr/bin/python
# -*- coding: utf-8 -*-

import crypt_key

CRYPTED_JPEG_MAGIC = 0xA6EF9107
CRYPTED_JPEG_MAGIC_STR = b'\x07\x91\xEF\xA6'  # 0x0791EFA6

CRYPTED_MODEL_DATA_MAGIC = 0x487B
CRYPTED_MODEL_DATA_MAGIC_STR = b'\x7B\x48'  # 0x7B48

DECRYPTED_MODEL_DATA_MAGIC = 0x0183
DECRYPTED_MODEL_DATA_MAGIC_STR = b'\x83\x01'  # 0x8301

CRYPTED_ZLIB_MAGIC = 0x32789755
CRYPTED_ZLIB_MAGIC_STR = b'\x55\x97\x78\x32'  # 0x55977832

DECRYPTED_ZLIB_MAGIC = 0x7468DEAD
DECRYPTED_ZLIB_MAGIC_STR = b'\xAD\xDE\x68\x74'  # 0xADDE6874


def decrypt(data):

    if not isinstance(data, bytearray):
        data = bytearray(data)

    i = 0
    k = len(data)

    j = 16

    while i < k:
        data[i] ^= crypt_key.GOOGLE_EARTH_CRYPT_KEY[j+8]

        i += 1
        j += 1

        if (j % 8) == 0:
            j += 16

        if j >= 1016:
            j += 8
            j %= 24

    return data


def crypt(data):
    return decrypt(data)


def iscrypted(data):

    if data.startswith(CRYPTED_JPEG_MAGIC_STR):
        return True

    elif data.startswith(CRYPTED_ZLIB_MAGIC_STR):
        return True

    elif data.startswith(CRYPTED_MODEL_DATA_MAGIC_STR):
        return True

    return False
