#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Usage:
    MasterRootPy unpack <FILE> <PATH>
    MasterRootPy pack (--xml|--proto) <PATH> <FILE>

Arguments:
    FILE        crypted dbRoot.v5 file
    PATH        path to unpacked dbRoot.v5 components

Options:
    --xml       pack dbRoot.v5 from XML
    --proto     pack dbRoot.v5 from ProtoBuffer
    -h, --help  show this message
"""

import sys
import logging

from docopt import docopt

from dbroot_tools import dbroot_open
from dbroot_xml import GoogleEarthdbRootv5Xml
from dbroot_proto import GoogleEarthdbRootv5Proto


def init_log(log_file, level=logging.NOTSET):

    logging.basicConfig(level=level,
                        format='%(asctime)s.%(msecs).03d %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        filename=log_file,
                        filemode='w')

    console = logging.StreamHandler()
    console.setLevel(level)
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


def main(args):
    args_file = args['<FILE>']
    args_path = args['<PATH>']

    if args_path[-1:] != '\\':
        args_path += '\\'

    if args['unpack']:
        logging.info('Start unpack file: {}'.format(args_file))
        dbroot = dbroot_open(args_file)
        if dbroot:
            dbroot.log_info()
            dbroot.save_dump(args_path)
            logging.info('Dump saved to folder: {}'.format(args_path))
        else:
            logging.error("Can't recognize dbRoot.v5 container type!")
            return 1

    elif args['pack']:
        if args['--xml']:
            dbroot = GoogleEarthdbRootv5Xml()
        elif args['--proto']:
            dbroot = GoogleEarthdbRootv5Proto()
        else:
            logging.error("Unexpected dbRoot.v5 container type!")
            return 1

        logging.info('Start dump loading...')
        dbroot.load_dump(args_path)
        logging.info('Dump loaded from folder: {}'.format(args_path))

        dbroot.log_info()

        dbroot.save_repack(args_file)
        logging.info('Repack saved to file: {}'.format(args_file))

    return 0


if __name__ == '__main__':
    try:
        init_log('MasterRootPy.log')
        arguments = docopt(__doc__)
        ret = main(arguments)
    except Exception as e:
        logging.exception(e)
        ret = 1
    sys.exit(ret)