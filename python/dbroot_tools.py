#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging

import os_tools
import crypt_key
import dbroot_xml
import dbroot_proto

DBROOT_TYPE_XML = 0
DBROOT_TYPE_GPB = 1
DBROOT_TYPE_UNK = 2

MAGIC_XML = b'\x94\x64\x87\x4E'
MAGIC_GPB = b'\x08'


def dbroot_type(dbroot):
    if dbroot.startswith(MAGIC_XML):
        return DBROOT_TYPE_XML
    elif dbroot.startswith(MAGIC_GPB):
        return DBROOT_TYPE_GPB
    else:
        return DBROOT_TYPE_UNK


def dbroot_open(dbroot_file):

    data = os_tools.read_bindata_from_file(dbroot_file)

    db_type = dbroot_type(data)

    if db_type == DBROOT_TYPE_XML:
        dbroot = dbroot_xml.GoogleEarthdbRootv5Xml()
    elif db_type == DBROOT_TYPE_GPB:
        dbroot = dbroot_proto.GoogleEarthdbRootv5Proto()
    else:
        dbroot = None

    if dbroot:
        dbroot.open(data)

    return dbroot


def dbroot_test_key(key):
    if key != bytearray(crypt_key.GOOGLE_EARTH_CRYPT_KEY[8:]):
        logging.warning('Unexpected KEY value!')