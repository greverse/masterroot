#!/usr/bin/python
# -*- coding: utf-8 -*-


import logging

from google.protobuf.descriptor import FieldDescriptor


PROTO_TYPES = ['int32', 'double', 'bytes', '', '', 'float', '', '']


def read_zigzag(data):

    if not isinstance(data, bytearray):
        data = bytearray(data)

    sz = 0
    val = 0
    for i in data:
        val += (i & 0x7F) << (sz * 7)
        sz += 1
        if i & 0x80 == 0:
            break

    return val, sz


def get_tag_type(tag):
    return tag & 0x07


def get_tag_number(tag):
    return (tag & 0xFFFFFFF8) >> 3


def get_tag_info(tag):
    return get_tag_number(tag), get_tag_type(tag)


def try_detect_bytes_type(data, def_type='bytes'):

    if data[0] == '\x12':
        return 'UTF8String'

    is_ascii_chars = False

    for i in data:
        if i > 127 or i < 0:
            is_ascii_chars = False
            break
        else:
            is_ascii_chars = True

    if is_ascii_chars:
        return 'string'
    else:
        return def_type


def get_unk_fields_desc(fields, msg_name):

    if len(fields) == 0:
        return None

    tab = '    '  # 4 spaces
    crlf = '\r\n'

    desc = crlf + msg_name + ' {' + crlf

    for field in fields:

        tag_number, tag_type = get_tag_info(read_zigzag(field[0])[0])
        tag_type_str = PROTO_TYPES[tag_type]

        if tag_type == 2:
            field_len, tag_size = read_zigzag(field[1])
            assert(len(field[1]) == field_len + tag_size)
            tag_type_str = try_detect_bytes_type(field[1][tag_size:])

        if tag_type_str == '':
            desc += '{0}{1}: {2}{3}'.format(tab, tag_number, tag_type, crlf)
        else:
            desc += '{0}repeated {1} field_{2:02d} = {2};{3}'.format(tab, tag_type_str, tag_number, crlf)

    return desc + '}' + crlf


def log_unknown_fields(msg, msg_name=None, print_field_number=False):

    if msg_name and msg_name != '':
        name = msg_name + '.'
    else:
        name = ''

    name += msg.DESCRIPTOR.name

    desc = get_unk_fields_desc(msg._unknown_fields, name)

    if desc:
        logging.info(desc)

    for descriptor, value in msg.ListFields():
        if descriptor.type == FieldDescriptor.TYPE_MESSAGE:
            if descriptor.label == FieldDescriptor.LABEL_REPEATED:
                if print_field_number:
                    i = 0
                    for field in value:
                        i += 1
                        log_unknown_fields(field, '{0}.{1}[{2}]'.format(name, descriptor.name, i))
                else:
                    for field in value:
                        log_unknown_fields(field, name)
            else:
                log_unknown_fields(value, name)
