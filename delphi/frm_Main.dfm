object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'MasterRoot'
  ClientHeight = 316
  ClientWidth = 559
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pgc1: TPageControl
    Left = 0
    Top = 0
    Width = 562
    Height = 124
    ActivePage = tsPack
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    object tsUnpack: TTabSheet
      Caption = 'Unpack'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 488
      ExplicitHeight = 0
      object lbl1: TLabel
        Left = 3
        Top = 3
        Width = 101
        Height = 13
        Caption = 'Input dbRoot.v5 file:'
      end
      object lbl2: TLabel
        Left = 3
        Top = 47
        Width = 133
        Height = 13
        Caption = 'Save components to folder:'
      end
      object edtOpenFileUnpack: TAdvFileNameEdit
        Left = 3
        Top = 22
        Width = 548
        Height = 21
        EmptyTextStyle = []
        Flat = False
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Font.Charset = DEFAULT_CHARSET
        Lookup.Font.Color = clWindowText
        Lookup.Font.Height = -11
        Lookup.Font.Name = 'Arial'
        Lookup.Font.Style = []
        Lookup.Separator = ';'
        Align = alCustom
        Anchors = [akLeft, akTop, akRight]
        Color = clWindow
        ReadOnly = False
        TabOrder = 0
        Visible = True
        Version = '1.3.5.0'
        ButtonStyle = bsButton
        ButtonWidth = 18
        Etched = False
        Glyph.Data = {
          CE000000424DCE0000000000000076000000280000000C0000000B0000000100
          0400000000005800000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D00000000DDD
          00000077777770DD00000F077777770D00000FF07777777000000FFF00000000
          00000FFFFFFF0DDD00000FFF00000DDD0000D000DDDDD0000000DDDDDDDDDD00
          0000DDDDD0DDD0D00000DDDDDD000DDD0000}
        FilterIndex = 0
        DialogOptions = []
        DialogKind = fdOpen
      end
      object diredtUnpack: TAdvDirectoryEdit
        Left = 3
        Top = 66
        Width = 548
        Height = 21
        EmptyTextStyle = []
        Flat = False
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Font.Charset = DEFAULT_CHARSET
        Lookup.Font.Color = clWindowText
        Lookup.Font.Height = -11
        Lookup.Font.Name = 'Arial'
        Lookup.Font.Style = []
        Lookup.Separator = ';'
        Align = alCustom
        Anchors = [akLeft, akTop, akRight]
        Color = clWindow
        ReadOnly = False
        TabOrder = 1
        Visible = True
        Version = '1.3.5.0'
        ButtonStyle = bsButton
        ButtonWidth = 18
        Etched = False
        Glyph.Data = {
          CE000000424DCE0000000000000076000000280000000C0000000B0000000100
          0400000000005800000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00F00000000FFF
          00000088888880FF00000B088888880F00000BB08888888000000BBB00000000
          00000BBBBBBB0B0F00000BBB00000B0F0000F000BBBBBB0F0000FF0BBBBBBB0F
          0000FF0BBB00000F0000FFF000FFFFFF0000}
        BrowseDialogText = 'Select Directory'
      end
    end
    object tsPack: TTabSheet
      Caption = 'Pack'
      ImageIndex = 1
      object lbl3: TLabel
        Left = 3
        Top = 47
        Width = 56
        Height = 13
        Caption = 'Pack to file:'
      end
      object lbl4: TLabel
        Left = 3
        Top = 3
        Width = 199
        Height = 13
        Caption = 'Input folder with dbRoot.v5 components:'
      end
      object lbl5: TLabel
        Left = 441
        Top = 3
        Width = 28
        Height = 13
        Align = alCustom
        Anchors = [akTop, akRight]
        Caption = 'Type:'
        ExplicitLeft = 400
      end
      object diredtPack: TAdvDirectoryEdit
        Left = 3
        Top = 22
        Width = 432
        Height = 21
        EmptyTextStyle = []
        Flat = False
        FocusColor = clBtnFace
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Font.Charset = DEFAULT_CHARSET
        Lookup.Font.Color = clWindowText
        Lookup.Font.Height = -11
        Lookup.Font.Name = 'Arial'
        Lookup.Font.Style = []
        Lookup.Separator = ';'
        Align = alCustom
        Anchors = [akLeft, akTop, akRight]
        Color = clWindow
        ReadOnly = False
        TabOrder = 0
        Visible = True
        Version = '1.3.5.0'
        ButtonStyle = bsButton
        ButtonWidth = 18
        Etched = False
        Glyph.Data = {
          CE000000424DCE0000000000000076000000280000000C0000000B0000000100
          0400000000005800000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00F00000000FFF
          00000088888880FF00000B088888880F00000BB08888888000000BBB00000000
          00000BBBBBBB0B0F00000BBB00000B0F0000F000BBBBBB0F0000FF0BBBBBBB0F
          0000FF0BBB00000F0000FFF000FFFFFF0000}
        BrowseDialogText = 'Select Directory'
      end
      object edtOutFilePack: TAdvFileNameEdit
        Left = 3
        Top = 66
        Width = 548
        Height = 21
        EmptyTextStyle = []
        Flat = False
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Font.Charset = DEFAULT_CHARSET
        Lookup.Font.Color = clWindowText
        Lookup.Font.Height = -11
        Lookup.Font.Name = 'Arial'
        Lookup.Font.Style = []
        Lookup.Separator = ';'
        Align = alCustom
        Anchors = [akLeft, akTop, akRight]
        Color = clWindow
        ReadOnly = False
        TabOrder = 1
        Visible = True
        Version = '1.3.5.0'
        ButtonStyle = bsButton
        ButtonWidth = 18
        Etched = False
        Glyph.Data = {
          CE000000424DCE0000000000000076000000280000000C0000000B0000000100
          0400000000005800000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D00000000DDD
          00000077777770DD00000F077777770D00000FF07777777000000FFF00000000
          00000FFFFFFF0DDD00000FFF00000DDD0000D000DDDDD0000000DDDDDDDDDD00
          0000DDDDD0DDD0D00000DDDDDD000DDD0000}
        FilterIndex = 0
        DialogOptions = []
        DialogKind = fdSave
      end
      object cbbType: TComboBox
        Left = 441
        Top = 22
        Width = 110
        Height = 21
        Align = alCustom
        Anchors = [akTop, akRight]
        ItemHeight = 13
        ItemIndex = 1
        TabOrder = 2
        Text = 'ProtoBuffer'
        Items.Strings = (
          'XML'
          'ProtoBuffer')
      end
    end
  end
  object mmoLog: TMemo
    Left = 0
    Top = 117
    Width = 560
    Height = 160
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object btnClearLog: TButton
    Left = 8
    Top = 283
    Width = 75
    Height = 25
    Align = alCustom
    Anchors = [akLeft, akBottom]
    Caption = 'Clear Log'
    TabOrder = 2
    OnClick = btnClearLogClick
  end
  object btnRun: TButton
    Left = 388
    Top = 283
    Width = 75
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Run'
    TabOrder = 3
    OnClick = btnRunClick
  end
  object btnExit: TButton
    Left = 476
    Top = 283
    Width = 75
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Exit'
    TabOrder = 4
    OnClick = btnExitClick
  end
end
