program MasterRoot;

uses
  Forms,
  frm_Main in 'frm_Main.pas' {frmMain},
  i_ConsoleAppRunner in 'i_ConsoleAppRunner.pas',
  u_ConsoleAppRunner in 'u_ConsoleAppRunner.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
