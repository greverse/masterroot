#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import struct
import logging

from google.protobuf.text_format import MessageToString, Merge
import dbroot_pb2

import os_tools
import dbroot_tools
from dbroot import GoogleEarthdbRootv5
from proto_tools import log_unknown_fields
from compress_tools import compress, decompress


class GoogleEarthdbRootv5Proto(GoogleEarthdbRootv5):

    __dbroot_raw_file = 'data.bin'
    __dbroot_raw_repack_file = 'data.repack.bin'
    __dbroot_key_file = 'key.bin'
    __dbroot_head_file = 'head.bin'
    __dbroot_proto_file = 'data.pb'
    __field_01_name = 'dbroot.MainContainer.field_01'

    def __init__(self):
        self.__field_01 = None
        self.__key = None
        self.__dbroot_raw = None
        self.__dbroot_proto = None

    def open(self, data):
        self.__parse_main_container(data)

    def __parse_main_container(self, data):
        self.__dbroot_proto = None

        proto = dbroot_pb2.MainContainer()
        proto.ParseFromString(data)

        log_unknown_fields(proto)

        self.__field_01 = proto.field_01
        self.__key = proto.key

        dbroot_tools.dbroot_test_key(proto.key)

        self.__parse_client_config(proto.data)

    def __parse_client_config(self, data):
        self.__dbroot_raw = decompress(data)

        proto = dbroot_pb2.ClientConfig()
        proto.ParseFromString(self.__dbroot_raw)

        log_unknown_fields(proto)

        self.__dbroot_proto = proto

    def load_dump(self, dump_path):
        head = os_tools.read_bindata_from_file(dump_path + self.__dbroot_head_file)
        self.__field_01 = struct.unpack_from('I', head)[0]

        self.__key = os_tools.read_bindata_from_file(dump_path + self.__dbroot_key_file)
        dbroot_tools.dbroot_test_key(self.__key)

        text = os_tools.read_bindata_from_file(dump_path + self.__dbroot_proto_file)
        proto = dbroot_pb2.ClientConfig()
        Merge(text, proto)
        self.__dbroot_proto = proto

        self.__dbroot_raw = proto.SerializeToString()

    def save_dump(self, dump_path):
        os_tools.make_dirs(dump_path)

        if not (self.__field_01 is None):
            head = struct.pack('I', self.__field_01)
            os_tools.save_bindata_to_file(dump_path + self.__dbroot_head_file, head)

        if self.__key:
            os_tools.save_bindata_to_file(dump_path + self.__dbroot_key_file, self.__key)

        if self.__dbroot_raw:
            os_tools.save_bindata_to_file(dump_path + self.__dbroot_raw_file, self.__dbroot_raw)

        if self.__dbroot_proto:
            os_tools.save_bindata_to_file(dump_path + self.__dbroot_proto_file,
                                          MessageToString(self.__dbroot_proto, as_utf8=True))

    def save_repack(self, out_file):
        proto = dbroot_pb2.MainContainer()

        proto.field_01 = self.__field_01
        proto.key = self.__key
        proto.data = str(compress(self.__dbroot_raw))
        repack = proto.SerializeToString()

        os_tools.make_dirs(out_file)
        os_tools.save_bindata_to_file(out_file, repack)

        os_tools.save_bindata_to_file(os.path.dirname(out_file) + '\\' + self.__dbroot_raw_repack_file, self.__dbroot_raw)

    def container_type(self):
        return dbroot_tools.DBROOT_TYPE_GPB

    def log_info(self):
        logging.info('Container Type: ProtoBuffer')
        logging.info('{}: {}'.format(self.__field_01_name, self.__field_01))
        logging.info('Version: {}'.format(self.version))

    def get_version(self):
        if self.__dbroot_proto:
            return self.__dbroot_proto.version.value
        else:
            return None

    def set_version(self, value):
        if self.__dbroot_proto:
            self.__dbroot_proto.version.value = value

    version = property(get_version, set_version)
