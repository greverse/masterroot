unit u_ConsoleAppRunner;
 
interface

uses
  Windows,
  Classes,
  i_ConsoleAppRunner;

type
  TConsoleAppRunner = class(TInterfacedObject, IConsoleAppRunner)
  private
    FIsEchoOff: Boolean;
    FOnOutputString: TOnOutputString;
    FOnProcessMessages: TOnProcessMessages;
    saSecurity: TSecurityAttributes;
    hNamedPipe: THandle;
    hFile: THandle;
    suiStartup: TStartupInfo;
    OverLapRd: OVERLAPPED;
    hEventRd: THANDLE;
    procedure InitPipe;
    procedure FinPipe;
    function OnProcessMessages: Boolean; inline;
  private
    { IConsoleAppRunner }
    function Run(
      const ACommand: string;
      const AParameters: string;
      const ACurrentDirectory: string
    ): Cardinal;
  public
    constructor Create(
      const AIsEchoOff: Boolean;
      const AOnOutputString: TOnOutputString;
      const AOnProcessMessages: TOnProcessMessages
    );
    destructor Destroy; override;
  end;

const
  cConsoleAppAbortedByUser = $FFFF;

implementation

uses
  SysUtils;

type
  EConsoleAppRunner = class(Exception);

const
  cBufferSize = 16384; // 16k
  cPipeName = '\\.\PIPE\ConsoleApp';
  cPipeMaxInstance = 100;
  cPipeDefaultTimeOut = 5000;

{ TConsoleAppRunner }

constructor TConsoleAppRunner.Create(
  const AIsEchoOff: Boolean;
  const AOnOutputString: TOnOutputString;
  const AOnProcessMessages: TOnProcessMessages
);
begin
  inherited Create;
  FIsEchoOff := AIsEchoOff;
  FOnOutputString := AOnOutputString;
  FOnProcessMessages := AOnProcessMessages;
  hNamedPipe := INVALID_HANDLE_VALUE;
  hFile := INVALID_HANDLE_VALUE;
  InitPipe;
end;

destructor TConsoleAppRunner.Destroy;
begin
  FinPipe;
  inherited Destroy;
end;

procedure TConsoleAppRunner.InitPipe;
begin
  saSecurity.nLength := SizeOf(TSecurityAttributes);
  saSecurity.bInheritHandle := True;
  saSecurity.lpSecurityDescriptor := nil;

  hNamedPipe := CreateNamedPipe(
    PChar(cPipeName),
    PIPE_ACCESS_DUPLEX or FILE_FLAG_OVERLAPPED,
    PIPE_WAIT or PIPE_TYPE_BYTE,
    cPipeMaxInstance,
    cBufferSize,
    cBufferSize,
    cPipeDefaultTimeOut,
    @saSecurity
  );
   
  if hNamedPipe = INVALID_HANDLE_VALUE then begin
    raise EConsoleAppRunner.Create(
      'CreateNamedPipe Fail! ' + SysErrorMessage(GetLastError)
    );
  end;

  hFile := CreateFile(
    PAnsiChar(cPipeName),
    GENERIC_WRITE or GENERIC_READ,
    FILE_SHARE_READ or FILE_SHARE_WRITE,
    nil,
    OPEN_EXISTING,
    FILE_FLAG_NO_BUFFERING or FILE_FLAG_OVERLAPPED,
    0
  );

  if hFile = INVALID_HANDLE_VALUE then begin
    raise EConsoleAppRunner.Create(
      'CreateFile Fail! ' + SysErrorMessage(GetLastError)
    );
  end;

  FillChar(suiStartup, SizeOf(TStartupInfo), #0);
  suiStartup.cb := SizeOf(TStartupInfo);
  suiStartup.hStdInput := GetStdHandle(STD_INPUT_HANDLE);
  suiStartup.hStdOutput := hNamedPipe;
  suiStartup.hStdError := hNamedPipe;
  suiStartup.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
  suiStartup.wShowWindow := SW_HIDE;

  hEventRd := CreateEvent(nil, True, False, nil);
  FillChar(OverLapRd, SizeOf(OVERLAPPED), #0);
  OverLapRd.hEvent := hEventRd;
end;

procedure TConsoleAppRunner.FinPipe;
begin
  if hFile <> INVALID_HANDLE_VALUE then begin
    CloseHandle(hFile);
  end;
  if hNamedPipe <> INVALID_HANDLE_VALUE then begin
    CloseHandle(hNamedPipe);
  end;
  CloseHandle(hEventRd);
end;

function TConsoleAppRunner.OnProcessMessages: Boolean;
begin
  if Assigned(FOnProcessMessages) then begin
    Result := FOnProcessMessages(Self);
  end else begin
    Result := True;
  end;
end;

function TConsoleAppRunner.Run(
  const ACommand: string;
  const AParameters: string;
  const ACurrentDirectory: string
): Cardinal;
var
  piProcess: TProcessInformation;
  pBuffer: array [0..cBufferSize] of AnsiChar;
  dRead: DWord;
  dTransRead: DWord;
  dRunning: DWord;
  sBuffer: string;
  bResult: Boolean;
  lastError: Cardinal;
  dWaitResult: DWORD;
  VProcessIsFinish: Boolean;
begin
  bResult := CreateProcess(
    nil,                                    // Application Name
    PChar(ACommand + ' ' + AParameters),    // Command Line
    @saSecurity,                            // Process Attributes
    @saSecurity,                            // Thread Attributes
    True,                                   // Inherit Handles
    NORMAL_PRIORITY_CLASS,                  // Creation Flags
    nil,                                    // Envinronment
    PChar(ACurrentDirectory),               // Current Directory
    suiStartup,                             // Startup Info
    piProcess                               // Process Info
  );
  if bResult then
  try
    if not FIsEchoOff then begin
      FOnOutputString(Self, '>> ' + ACommand + ' ' + AParameters + ' [' + ACurrentDirectory + ']' + #$D#$A#$D#$A);
    end;
    repeat
      dRunning := WaitForSingleObject(piProcess.hProcess, 100);
      repeat
        dRead := 0;
        bResult := ReadFile(hFile, pBuffer[0], cBufferSize, dRead, @OverLapRd);
        if not bResult then begin
          lastError := GetLastError;
          if lastError = ERROR_IO_PENDING then begin
            VProcessIsFinish := False;
            repeat
              dWaitResult := WaitForSingleObject(hEventRd, 50);
              if dWaitResult = WAIT_OBJECT_0 then begin
                Break;
              end else begin
                if VProcessIsFinish then begin
                  if not FIsEchoOff then begin
                    FOnOutputString(Self, '<< ExitCode = ' + IntToStr(Result) + #$D#$A#$D#$A);
                  end;
                  Exit;
                end;
                if GetExitCodeProcess(piProcess.hProcess, Result) then begin
                  VProcessIsFinish := Result <> STILL_ACTIVE;
                end;
                if not OnProcessMessages then begin
                  Result := cConsoleAppAbortedByUser;
                  Exit;
                end;
              end;
            until False;
          end else if lastError = ERROR_BROKEN_PIPE then begin
            raise EConsoleAppRunner.Create(
              'ReadFile: Pipe Broken! ' + SysErrorMessage(GetLastError)
            );
          end else begin
            raise EConsoleAppRunner.Create(
              'ReadFile: Fail! ' + SysErrorMessage(GetLastError)
            );
          end;
        end;
        if GetOverlappedResult(hFile, OverLapRd, dTransRead, False) then begin
          if dTransRead <> 0 then begin
            if Assigned(FOnOutputString) then begin
              pBuffer[dTransRead] := #0;
              sBuffer := StringReplace(
                AnsiString(pBuffer),
                #$D#$D#$A,
                #$D#$A,
                [rfReplaceAll, rfIgnoreCase]
              );
              FOnOutputString(Self, sBuffer + #$D#$A);
            end;
          end else begin
            Assert(False, 'dTransRead = 0');
          end;
        end else begin
          Assert(False, 'GetOverlappedResult Fail!');
        end;
        if not OnProcessMessages then begin
          Result := cConsoleAppAbortedByUser;
          Exit;
        end;
      until (dRead < cBufferSize);
    until (dRunning <> WAIT_TIMEOUT);
    if not GetExitCodeProcess(piProcess.hProcess, Result) then begin
      raise EConsoleAppRunner.Create('GetExitCodeProcess Fail!');
    end;
    if not FIsEchoOff then begin
      FOnOutputString(Self, '<< ExitCode = ' + IntToStr(Result) + #$D#$A#$D#$A);
    end;
  finally
    CloseHandle(piProcess.hProcess);
    CloseHandle(piProcess.hThread);
  end; 
end;

end.
