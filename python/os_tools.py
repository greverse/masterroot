#!/usr/bin/python
# -*- coding: utf-8 -*-

import os


def make_dirs(path):
    path = path.replace('\\', '/')
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))


def save_bindata_to_file(filename, bindata):
    with open(filename, 'wb') as f:
        f.write(bindata)


def read_bindata_from_file(filename):
    with open(filename, 'rb') as f:
        return f.read()
