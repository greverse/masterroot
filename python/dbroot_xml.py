#!/usr/bin/python
# -*- coding: utf-8 -*-

import struct
import logging

import os_tools
import dbroot_tools
from dbroot import GoogleEarthdbRootv5
from compress_tools import compress, decompress


class GoogleEarthdbRootv5Xml(GoogleEarthdbRootv5):

    __dbroot_head_file = 'head.bin'
    __dbroot_key_file = 'key.bin'
    __dbroot_xml_file = 'data.xml'

    def __init__(self):
        self.__magic = None
        self.__unk = None
        self.__version = None
        self.__key = None
        self.__xml = None

    @staticmethod
    def __unpack_head(data):
        return struct.unpack('<IHH', data[:8])

    def __get_packed_head(self):
        return struct.pack('<IHH', self.__magic, self.__unk, self.__version)

    def open(self, data):
        self.__magic, self.__unk, self.__version = self.__unpack_head(data)
        self.__key = data[8:1024]
        self.__xml = decompress(data[1024:])

        dbroot_tools.dbroot_test_key(self.__key)

    def load_dump(self, dump_path):
        head = os_tools.read_bindata_from_file(dump_path + self.__dbroot_head_file)
        self.__magic, self.__unk, self.__version = self.__unpack_head(head)

        self.__key = os_tools.read_bindata_from_file(dump_path + self.__dbroot_key_file)
        dbroot_tools.dbroot_test_key(self.__key)

        self.__xml = os_tools.read_bindata_from_file(dump_path + self.__dbroot_xml_file)

    def save_dump(self, dump_path):
        os_tools.make_dirs(dump_path)

        head = self.__get_packed_head()
        os_tools.save_bindata_to_file(dump_path + self.__dbroot_head_file, head)

        if self.__key:
            os_tools.save_bindata_to_file(dump_path + self.__dbroot_key_file, self.__key)

        if self.__xml:
            os_tools.save_bindata_to_file(dump_path + self.__dbroot_xml_file, self.__xml)

    def save_repack(self, out_file):
        head = self.__get_packed_head()
        repack = head + self.__key + compress(self.__xml)
        os_tools.save_bindata_to_file(out_file, repack)

    def container_type(self):
        return dbroot_tools.DBROOT_TYPE_XML

    def log_info(self):
        logging.info('Container Type: XML')
        logging.info('UnkField: {}'.format(hex(self.__unk)))
        logging.info('Version: {}'.format(self.version))

    def get_version(self):
        if not (self.__version is None):
            return self.__version ^ 0x4200
        else:
            return None

    def set_version(self, value):
        self.__version = value ^ 0x4200

    version = property(get_version, set_version)
