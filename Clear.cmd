
cd .\delphi\
rmdir /s /q dcu

cd ..

del /s *.~*  2> nul
del /s *.dcu 2> nul
del /s *.map 2> nul
del /s *.local 2> nul
del /s *.identcache 2> nul
del /s *.drc 2> nul
del /s *.log 2> nul
del /s *.pyc 2> nul
