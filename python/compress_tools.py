#!/usr/bin/python
# -*- coding: utf-8 -*-

import zlib
import struct

import crypt_tools


def decompress(data, do_decrypt=True):

    if do_decrypt:
        if crypt_tools.iscrypted(data):
            data = str(crypt_tools.decrypt(data))

    magic = struct.unpack_from("L", data[:4])[0]

    if magic == crypt_tools.DECRYPTED_ZLIB_MAGIC:
        uncompressed_size = struct.unpack_from("I", data[4:8])[0]
        uncompressed_data = zlib.decompress(data[8:])
        assert(len(uncompressed_data) == uncompressed_size)
        return uncompressed_data
    else:
        return data


def compress(data, do_crypt=True):

    head = struct.pack('LI', crypt_tools.DECRYPTED_ZLIB_MAGIC, len(data))

    data = zlib.compress(data)

    if not do_crypt:
        return head + data
    else:
        return crypt_tools.crypt(head + data)