#!/usr/bin/python
# -*- coding: utf-8 -*-

import abc


class GoogleEarthdbRootv5(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def open(self, data):
        return

    @abc.abstractmethod
    def load_dump(self, dump_path):
        return

    @abc.abstractmethod
    def save_dump(self, dump_path):
        return

    @abc.abstractmethod
    def save_repack(self, out_file):
        return

    @abc.abstractproperty
    def container_type(self):
        return

    @abc.abstractmethod
    def log_info(self):
        return

    def get_version(self):
        return

    def set_version(self, value):
        return

    version = abc.abstractproperty(get_version, set_version)
